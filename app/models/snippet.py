from pydantic import Field, BaseModel


class Snippet(BaseModel):
    trigger: str = Field(..., "trigger")
    description: str = Field(..., "description")
    text: str = Field(..., "text")
