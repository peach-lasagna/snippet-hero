import os

from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())


allow_origins = os.getenv("CORS_ORIGINS")
