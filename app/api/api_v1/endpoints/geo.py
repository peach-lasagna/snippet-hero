from typing import List

import aiohttp
from fastapi import APIRouter

from app.models import GeoObject

from ....core.settings import google_map_key

router = APIRouter()


@router.get("/", response_model=List[str])
async def get_points(info: GeoObject, ):
    """Используйте для получения адресов объектов, подходящих по text

    Args:
        text (str): текст для поиска

    Returns:
        (List[str]) Список адресов полученных объектов
    """
    payload_textsearch = {
        "key": google_map_key,
        "query": info.text,
        "language": "ru"
    }
    async with aiohttp.ClientSession() as session:
        async with session.get("https://maps.googleapis.com/maps/api/place/textsearch/json",
                               params=payload_textsearch) as resp:
            res1 = [obj["formatted_address"] for obj in (await resp.json())["results"]]
        return res1
