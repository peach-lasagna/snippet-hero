from fastapi import APIRouter

from .api_v1.endpoints import near

api_router_v1 = APIRouter()

api_router_v1.include_router(near.router,)
