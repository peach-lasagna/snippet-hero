import logging

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .api.api import api_router_v1
from .middlewares import CashMiddleware, RouteLoggerMiddleware
from .core.settings import allow_origins

logger = logging.getLogger(__name__)

app = FastAPI(
    title="Snippet Hero",
    debug=True,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=allow_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(
    RouteLoggerMiddleware,
    logger=logger,
)

app.include_router(api_router_v1, prefix="/api/v1")
